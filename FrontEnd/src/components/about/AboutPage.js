import React from 'react';
import { Link } from 'react-router-dom';

function AboutPage() {
	return (
		<div className='jumbotron'>
			<h1>BOOKS BOOKS BOOKS</h1>

			
			<Link to='/about'>
					<button className='btn btn-danger'>About</button>
			
				</Link>

				<Link to='/book/create'>
					<button className='btn btn-danger'>Add Book</button>
			
				</Link>

				<Link to='/book/delete'>
					<button className='btn btn-danger'>Delete</button>
				</Link>

				<Link to='/book/Update'>
					<button className='btn btn-danger'>Update</button>
				</Link>

				<h4><br></br>
				“The reading of all good books is like conversation with the finest (people) of the past centuries.” – Descartes
			</h4>
	
			
		</div>
	);
}

export default AboutPage;
