import React from 'react';
import { Link } from 'react-router-dom';

//import PostList from './components/book/PostList'

export default class HomePage extends React.Component {
	render() {
		return (
			<div className='jumbotron'>
				<h1>Book Store</h1>
				<Link to='/about'>
					<button className='btn btn-danger'>About</button>
			
				</Link>

				<Link to='/book/create'>
					<button className='btn btn-danger'>Add Book</button>
			
				</Link>

				<Link to='/book/delete'>
					<button className='btn btn-danger'>Delete</button>
				</Link>

				<Link to='/book/Update'>
					<button className='btn btn-danger'>Update</button>
				</Link>


		
			</div>
		);
	}
}
