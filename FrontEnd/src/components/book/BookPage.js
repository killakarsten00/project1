import React from 'react';
import BookList from './BookList';
import { Link } from 'react-router-dom';

export default class BookPage extends React.Component {
	// optionally, you could set state in the constructor
	// constructor(){
	//     super();
	//     this.state ={
	//         book:[]
	//     }
	// }

	state = {
		book: [],
	};

	componentDidMount() {
		fetch('http://localhost:4000/book')
			.then((response) => response.json())
			.then((data) => {
				this.setState({
					book: data,
				});
			})
			.catch((err) => console.log(err));
	}

	render() {
		return (
			<div className='jumbotron'>

				<h1>Book Page</h1>

				<Link to='/about'>
					<button className='btn btn-danger'>About</button>
			
				</Link>

				<Link to='/book/create'>
					<button className='btn btn-danger'>Add Book</button>
			
				</Link>

				<Link to='/book/delete'>
					<button className='btn btn-danger'>Delete</button>
				</Link>

				<Link to='/book/Update'>
					<button className='btn btn-danger'>Update</button>
				</Link>
	
			</div>
		);
	}
}

