import React from 'react';
import TextInput from '../common/TextInput';

export default function DeleteBookForm(props) {
	return (
		<form onSubmit={props.onSubmit}>
			<TextInput
				id='id'
				label='id'
				onChange={props.onChange}
				onSubmit={props.onSubmit}
				name='id'
                value={props.book.Book}
                type='number'
			/>

			<button type='submit' className='btn btn-primary'>
				Submit
			</button>
		</form>
	);
}
