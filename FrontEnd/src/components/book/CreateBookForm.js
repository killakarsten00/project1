import React from 'react';
import TextInput from '../common/TextInput';
// WORKS
export default function CreateBookForm(props) {
	return (
		<form onSubmit={props.onSubmit}>
			
			<TextInput
			id="id"
			label="id"
			onChange={props.onChange}
			onSubmit={props.onSubmit}
			name="id"
			value={props.book.id}
			type="number"
			/>


            <TextInput
				id='booktitle'
				label='booktitle'
				onChange={props.onChange}
				onSubmit={props.onSubmit}
				name='booktitle'
				value={props.book.booktitle}
	
			/>

            <TextInput
				id='price'
				label='price'
				onChange={props.onChange}
				onSubmit={props.onSubmit}
				name='price'
				value={props.book.price}
				type='number'
			/>
            
            <TextInput
				id='author'
				label='author'
				onChange={props.onChange}
				onSubmit={props.onSubmit}
				name='author'
				value={props.book.author}
			/>

            <TextInput
				id='publisher'
				label='publisher'
				onChange={props.onChange}
				onSubmit={props.onSubmit}
				name='publisher'
				value={props.book.publishers}
			/>


			


			<button type='submit' className='btn btn-primary'>
				Submit
			</button>
		</form>
	);
}
