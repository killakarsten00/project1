import React, { useState } from 'react';
import DeleteBookForm from './DeleteBookForm';
import { toast } from 'react-toastify';


export default function DeleteBook(props) {
	const [book, setBook] = useState({
		id: '',
	});

	function handleChange(event) {
		setBook({ ...book, [event.target.name]: event.target.value });
	}

	function handleSubmit(event) {
		event.preventDefault();
		fetch('http://localhost:4000/book/' + book.id, {
			method: 'DELETE',
			headers: { 'Content-Type': 'application/json' },
		})
			.then((result) => {
				toast.success('Book Deleted!');
				props.history.push('/book' + book.id);
			})
			.catch((err) => {
				toast.error('Something went wrong....');
			});
	}

	return (
		<div className='jumbotron'>
			<h3>Delete a Book Below</h3>
			<DeleteBookForm
				onSubmit={handleSubmit}
				onChange={handleChange}
				book={book}
			/>
		</div>
	);
}
