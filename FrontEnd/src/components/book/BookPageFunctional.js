import React, { useState, useEffect } from 'react';


export default function BookPage() {

	//papers instead of paser

	const [books, setBook] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/book')
			.then((response) => response.json())
			.then((data) => {
				setBook(data);
			})
			.catch((err) => console.log(err));
	}, []);

	return (
		<div>
			<h3>Book Page</h3>

		</div>
	);
}
