
import React, { Component } from 'react'
import axios from 'axios'


class PostList extends Component {
	constructor(props) {
		super(props)

		this.state = {
      posts: [],
      errorMsg: ''
		}
	}

	componentDidMount() {
		axios
			.get('book')
			.then(response => {
				console.log(response)
				this.setState({ posts: response.data })
			})
			.catch(error => {
        console.log(error)
        this.setState({errorMsg: 'Error retrieving data'})
			})
	}

	render() {
        const { posts, errorMsg } = this.state
        
		return (
		
		<div class="page">

		<div class="card">
		
			<div class="card-header">
				<p> <i class="fa fa-bars"></i> <i class="fa fa-calendar ml-4" aria-hidden="true"></i> <i class="fa fa-star ml-4" aria-hidden="true"></i> <span class="float-right"> <span class="mr-4 navTask">Task</span> <span class="mr-4">Archive</span> <i class="fa fa-search" aria-hidden="true"></i> </span> </p>
			</div>
		
			<div class="card-body">
				<p class="heading1"> <span class="today">Available Books</span> </p>

				{posts.map(Book => (
				<p key={Book.id}>Title: {Book.booktitle} | Price: {Book.price} | Author: {Book.author} | Publisher: {Book.publisher} <i class="fa fa-calendar mr-2" aria-hidden="true"></i>
				
				</p>
				))}


				
				
			
				

				
			</div>
		</div>
	</div>







		)
	}
}

export default PostList