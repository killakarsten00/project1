import React, { useState } from 'react';
import UpdateBookForm from './UpdateBookForm';
import { toast } from 'react-toastify';

export default function Updatebook(props) {
    const [book, setbook] = useState({
        id: '',
        title: '',
        price: '',
        author: '',
        publisher: '',

    });

    function handleChange(event) {
        setbook({ ...book, [event.target.name]: event.target.value });
    }

    function handleSubmit(event) {
        event.preventDefault();
        fetch('http://localhost:4000/book' + book.id, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({

                "title": book.booktitle,
                "price": book.price,
                "author": book.author,
                "publisher": book.publisher,

            }),
        })
            .then((result) => {
                toast.success('New book Created!');
                props.history.push('/book' + book.id);
            })
            .catch((err) => {
                toast.error('Something went wrong....');
            });
    }


    return (
        <div className='jumbotron'>
            <h1>Update a Book</h1>
            <UpdateBookForm
                onSubmit={handleSubmit}
                onChange={handleChange}
                book={book}
            />
        </div>
    );
}
