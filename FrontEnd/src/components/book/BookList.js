import React from 'react';

export default function BookList(props) {
	return (
		<table className='table'>
			<thead>
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Price</th>
					<th>Author</th>
					<th>Publisher</th>
				</tr>
			</thead>
			<tbody>
				{props.book.map((book) => {
					return (
						<tr key={book.id}>
							<td>{book.id}</td>
							<td>{book.booktitle}</td>
                            <td>{book.price}</td>
							<td>{book.author}</td>
							
							<td>{book.publisher}</td>
				
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}
