import React, { useState } from 'react';
import CreateBookForm from './CreateBookForm';
import { toast } from 'react-toastify';

export default function CreateBook(props) {
	const [book, setBook] = useState({
		id: '',
		booktitle: '',
		price: '',
		author: '',
		publisher: '',
	});

	function handleChange(event) {
        setBook({ ...book, [event.target.name]: event.target.value });
        console.log(book);
	}

	function handleSubmit(event) {
		event.preventDefault();
		fetch('/book', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(book),
		})
			.then((result) => {
				toast.success('New Book Created!');
				props.history.push('/book');
			})
			.catch((err) => {
				toast.error('Something went wrong....');
			});
	}

	return (
		<div className='jumbotron'>
			
			<h3>Create a New Book Below</h3>
			<CreateBookForm
				onSubmit={handleSubmit}
				onChange={handleChange}
				book={book}
			/>
		</div>
	);
}
