import React from 'react';


/*
				id={props.id}
				title={props.booktitle}
				price={props.price}
				author={props.author}
				publisher={props.publisher}
				*/


export default function TextInput(props) {
	return (
		<div className='form-group'>
			<label htmlFor={props.label}>{props.label} :</label>
			<input
				type={props.type}
				className='form-control'
				placeholder={`Enter ${props.name}`}
				

				id={props.id}
				name={props.name}
				value={props.value}

				
				onChange={props.onChange}
			/>
		</div>
	);
}
