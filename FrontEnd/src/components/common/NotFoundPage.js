import React from 'react';
import { Link } from 'react-router-dom';

function NotFoundPage() {
	return (
		<>
			<h1>404. Page not found (you asked for the wrong resource)</h1>
			<Link to='/'>
				<button className='btn btn-warning'>Take Me Home</button>
			</Link>
		</>
	);
}

export default NotFoundPage;
