import React from 'react';
import { NavLink } from 'react-router-dom';

import { MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon } from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';

const Navbar = () => {
	const bgPink = {backgroundColor: '#e91e63'}
	const activeStyle = { color: 'Black' };
	return (
		<MDBNavbar style={bgPink} dark expand="md" scrolling fixed="top"> 
			<MDBNavbarBrand href="/">
                  <strong>Books & Kicks</strong>
              </MDBNavbarBrand>
	
		<nav>
                
			<NavLink to='/' activeStyle={activeStyle} exact>
				Home
			</NavLink>
			
			{' | '}
			<NavLink to='/book' activeStyle={activeStyle}>
				Book
			</NavLink>
			{' | '}
			<NavLink to='/about' activeStyle={activeStyle}>
				About
			</NavLink>
		</nav>
		</MDBNavbar>
	
	

	)};

export default Navbar;





