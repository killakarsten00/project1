import React from 'react';
import HomePage from './components/home/HomePage';
import './App.css';
import AboutPage from './components/about/AboutPage';
import BookPage from './components/book/BookPage';
import { Switch, Route } from 'react-router-dom';
import Navbar from './components/common/Navbar';
//import NotFoundPage from './components/common/NotFoundPage';
import CreateBook from './components/book/CreateBook';
import DeleteBook from './components/book/DeleteBook';
import UpdateBook from './components/book/UpdateBook';

import { ToastContainer } from 'react-toastify';

import PostList from './components/book/PostList';

function App() {
	return (
		<div className='container-fluid'>
			<Navbar />
			<ToastContainer autoClose={3000} hideProgressBar />
			<Switch>
				<Route path='/' component={HomePage} exact />
				<Route path='/about' component={AboutPage} />
				<Route path='/book/create' component={CreateBook} />
				<Route path='/book/delete' component={DeleteBook} />
				<Route path='/book/update' component={UpdateBook} />
				<Route path='/book' component={BookPage} />

			</Switch>
			
			<PostList /> 
		</div>
	);
}

export default App;
